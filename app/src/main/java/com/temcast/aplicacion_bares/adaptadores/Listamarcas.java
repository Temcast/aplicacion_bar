package com.temcast.aplicacion_bares.adaptadores;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.temcast.aplicacion_bares.R;
import com.temcast.aplicacion_bares.tipos.ListaCervezas;

import java.util.ArrayList;

/**
 * Created by Borja on 09/03/2017.
 */

public class Listamarcas extends BaseExpandableListAdapter{
    ArrayList <ListaCervezas> lc = new ArrayList<>();
    Context c;

    public Listamarcas (ArrayList <ListaCervezas> lc, Context c){
        this.lc = lc;
        this.c = c;
    }

    @Override
    public int getGroupCount() {
        return lc.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return lc.get(groupPosition).getLista().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return lc.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return lc.get(groupPosition).getLista().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(c).inflate(R.layout.activity_marcas_titulo,null);
        }
        ((TextView) convertView.findViewById(R.id.Marcas_marca)).setText(lc.get(groupPosition).marca);
                return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(c).inflate(R.layout.activity_marcas__modelo,null);
        }
        ((TextView) convertView.findViewById(R.id.marcas_modelo)).setText(lc.get(groupPosition).getLista().get(childPosition).modelo);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

package com.temcast.aplicacion_bares.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.temcast.aplicacion_bares.R;
import com.temcast.aplicacion_bares.tipos.ListaCervezas;

import java.util.ArrayList;

/**
 * Created by Jorge on 9/3/17.
 */

public class ListaAdapterCervezas extends BaseExpandableListAdapter {
    ArrayList<ListaCervezas> lista;
    Context c;
    public void Listacervezas(ArrayList<ListaCervezas> lista, Context c){
        this.lista=lista;
        this.c=c;
    }
    @Override
    public int getGroupCount() {
        return lista.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return lista.get(i).getLista().size();
    }

    @Override
    public Object getGroup(int i) {
        return lista.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return lista.get(i).getLista().get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if(view==null){
            LayoutInflater l=(LayoutInflater)this.c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=l.inflate(R.layout.activity_elemento_grupo_cervezas,null);
        }

        return null;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }


}

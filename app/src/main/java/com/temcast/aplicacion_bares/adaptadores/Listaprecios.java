package com.temcast.aplicacion_bares.adaptadores;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.temcast.aplicacion_bares.R;
import com.temcast.aplicacion_bares.tipos.Precio;

import java.util.ArrayList;

/**
 * Created by Jorge on 08/03/2017.
 */

public class Listaprecios extends BaseAdapter {
    ArrayList<Precio> lista=new ArrayList<>();
    Context c;

    public Listaprecios(ArrayList<Precio> lista, Context c){
        this.lista=lista;
        this.c=c;
    }
    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return lista.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ConstraintLayout l;
        if(convertView==null){
            l= (ConstraintLayout) LayoutInflater.from(c).inflate(R.layout.activity_elemento_precios,null);
        }else{
            l=(ConstraintLayout) convertView;
        }
        ((TextView) l.findViewById(R.id.Bar_nombre)).setText(lista.get(position).nombre);
        ((TextView)l.findViewById(R.id.bar_precio)).setText(lista.get(position).precio);
        ((TextView)l.findViewById(R.id.Bar_distancia)).setText(lista.get(position).distancia);
        return l;
    }
}

package com.temcast.aplicacion_bares;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Menu_principal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
    }
    public void acciones(View v){
        switch (v.getId()){
            case R.id.Opcion1:
                startActivity(new Intent(this,Menu_marcas.class));
                break;
            case R.id.Opcion2:
                startActivity(new Intent(this,Menu_precios.class));
                break;
            case R.id.Opcion3:
                startActivity(new Intent(this,Menu_mapas.class));
                break;
            case R.id.Opcion4:
                startActivity(new Intent(this,null));
                break;
        }
    }
}

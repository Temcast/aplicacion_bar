package com.temcast.aplicacion_bares;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;

import com.temcast.aplicacion_bares.Cargador_datos.Datos_iniciales;
import com.temcast.aplicacion_bares.adaptadores.Listamarcas;
import com.temcast.aplicacion_bares.tipos.Cervezas;
import com.temcast.aplicacion_bares.tipos.ListaCervezas;

import java.util.ArrayList;

public class Menu_marcas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_marcas);
        ArrayList <ListaCervezas> l = Datos_iniciales.getListaCervezas();
        Listamarcas lm = new Listamarcas(l,this);
        ((ExpandableListView) findViewById(R.id.marcas_general)).setAdapter(lm);
    }
}

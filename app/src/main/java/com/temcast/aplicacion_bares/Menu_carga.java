package com.temcast.aplicacion_bares;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.temcast.aplicacion_bares.Cargador_datos.Datos_iniciales;
import com.temcast.aplicacion_bares.tipos.Cervezas;
import com.temcast.aplicacion_bares.tipos.ListaCervezas;
import com.temcast.aplicacion_bares.tipos.Precio;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;


public class Menu_carga extends AppCompatActivity {
    private RequestQueue cola;
    final CountDownLatch cont=new CountDownLatch(4);
    String version_server;
    String version_server_cerv;
    String version_server_precios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menucarga);
        cola= Volley.newRequestQueue(this);
        comprobaciones();
    }
    public void comprobaciones(){


        new Thread(new Runnable() {
            @Override
            public void run() {
                conexion();
            }
        }).start();//comprueba que tengas una conexion activaa de interent
        new Thread(new Runnable() {
            @Override
            public void run() {
                Version_min();
            }
        }).start();//Comprueba todas las versiones de los diferentes datos

       new Thread(new Runnable() {
            @Override
            public void run() {//Despues de finalizar todos los hilos este metodo continuara con la ejecucuin
                try {
                    cont.countDown();
                    cont.await();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            continuar();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }



    private void conexion() {
        StringRequest st= new StringRequest(Request.Method.HEAD, "http://www.google.es", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                cont.countDown();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        error_red();
                    }
                });
            }
        });
        cola.add(st);
    }
    private void Guardado_local_cer(String response){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefEditor = sharedPref.edit();
        prefEditor.putString( "cervezas", response );
        prefEditor.commit();
    }
    private void Guardado_local_pre(String response){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor prefEditor = sharedPref.edit();
        prefEditor.putString( "precio", response );
        prefEditor.commit();
    }

    private void Version_min(){
        StringRequest vn=new StringRequest(Request.Method.GET, "http://temcast.ddns.net/checks/Version_min.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json=new JSONObject(response);
                    version_server=json.getString("version_min");
                    version_server_cerv=json.getString("version_cerv");
                    version_server_precios=json.getString("version_precios");
                    String version_datos="0.1";
                     if(!version_server.equals(version_datos)){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                error_min();
                            }
                        });
                    }else {
                    cont.countDown();
                         new Thread(new Runnable() {
                             @Override
                             public void run() {
                                 Version_datos();
                             }
                         }).start();
                         new Thread(new Runnable() {
                             @Override
                             public void run() {
                                 Version_precios();
                             }
                         }).start();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                finally {

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        error_serv();
                    }
                });
            }
        });
        cola.add(vn);


    }
    private void Version_precios() {

        String String_URL = "http://temcast.ddns.net/checks/Get_precios.php";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String version_datos=prefs.getString("version_datos","0.0");
        if(!version_server_precios.equals(version_datos)){
            StringRequest string_cer_serv = new StringRequest(String_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Guardado_local_pre(response);
                    cons_listaprec();


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
            );
            cola.add(string_cer_serv);
        }
        cont.countDown();

    }
    private void Version_datos() {
        String String_URL = "http://temcast.ddns.net/checks/Get_cervezas.php";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String version_datos=prefs.getString("version_datos","0.0");
        if(!version_server_cerv.equals(version_datos)){
            StringRequest string_cer_serv = new StringRequest(String_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Guardado_local_cer(response);
                    cons_listacerv();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }
        );
            cola.add(string_cer_serv);
        }
        cont.countDown();
    }
    public void cons_listacerv(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String response=prefs.getString("cervezas","");

        try {
            ArrayList<ListaCervezas> lista=Datos_iniciales.getListaCervezas();
            JSONObject Cer = new JSONObject(response);
            for(int x=0;x<Cer.getInt("Longitud");x++){
                JSONObject jo=new JSONObject(Cer.getString(Integer.toString(x)));
                Cervezas ce=new Cervezas(jo.getString("Modelo"),jo.getString("Tamano"),jo.getString("Tipo"),jo.getString("Descripcion"),jo.getString("Origen"));
                boolean esta=false;

                for(ListaCervezas l:lista){
                    if(l.marca.equals(jo.getString("Marca"))){
                        l.getLista().add(ce);
                        esta=true;
                    }
                }
                if (!esta){
                    ListaCervezas lo=new ListaCervezas();
                    lo.marca=jo.getString("Marca");
                    lo.añadircerveza(ce);
                    lista.add(lo);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public  void cons_listaprec(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String response=prefs.getString("precio","");
        try {
            JSONObject precios = new JSONObject(response);
            JSONArray lprecios=precios.optJSONArray("Precios");
            for(int x=0;x<lprecios.length();x++){
                JSONObject t=lprecios.getJSONObject(x);
                Precio p=new Precio(t.getString("Nombre"),t.getString("Precio"),t.getString("Distancia"));
                Datos_iniciales.Get_Lprecio().add(p);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void continuar() {
    startActivity(new Intent(this,Menu_principal.class));
   }

    private void error_min(){
        AlertDialog.Builder al=new AlertDialog.Builder(this);
        al.setMessage("Para continuar usando la aplicacion por favor actualicela").setCancelable(false).setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        }).show();
    }
    private void error_red(){
        AlertDialog.Builder al=new AlertDialog.Builder(this);
        al.setMessage("Para continuar usando la aplicacion necesita una conexión activa de internet").setCancelable(false).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.exit(0);
            }
        }).show();
    }
    private void error_serv(){
        AlertDialog.Builder al=new AlertDialog.Builder(this);
        al.setMessage("Nuestro servidor puede estar sufriendo alguna averia, por favor intentelo de nuevo pasados unos minutos.").setCancelable(false).setNegativeButton("aceptar",null).show();
    }
}

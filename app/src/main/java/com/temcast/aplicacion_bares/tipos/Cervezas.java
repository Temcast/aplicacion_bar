package com.temcast.aplicacion_bares.tipos;

/**
 * Created by Jorge on 08/03/2017.
 */

public class Cervezas {
    public String modelo,tamaño,tipo,descripcion,origen;
    public Cervezas(String modelo,String tamaño, String tipo,String descripcion,String origen){
        this.modelo=modelo;
        this.tipo=tipo;
        this.tamaño=tamaño;
        this.descripcion=descripcion;
        this.origen=origen;
    }
}

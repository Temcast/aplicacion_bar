package com.temcast.aplicacion_bares.tipos;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.util.ArrayList;

/**
 * Created by Jorge on 08/03/2017.
 */

public class ListaCervezas {
    public String marca;
    private ArrayList<Cervezas> lista=new ArrayList<>();


    public void añadircerveza(Cervezas cerv) {
        if(!lista.contains(cerv))lista.add(cerv);
    }

    public ArrayList<Cervezas> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Cervezas> lista) {
        this.lista = lista;
    }


    public ListaCervezas(){
        this.marca=marca;
    }



}

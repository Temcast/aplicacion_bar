package com.temcast.aplicacion_bares;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.temcast.aplicacion_bares.Cargador_datos.Datos_iniciales;
import com.temcast.aplicacion_bares.adaptadores.Listaprecios;
import com.temcast.aplicacion_bares.tipos.Precio;

import java.util.ArrayList;

public class Menu_precios extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bares_lista);
        /*ArrayList<Precio> bares=new ArrayList<>();
        Precio b=new Precio("lucas","85€","52");
        bares.add(b);
        b=new Precio("lucas","85€","52");
        bares.add(b);
        b=new Precio("lucas","85€","52");
        bares.add(b);
        b=new Precio("lucas","832€","522");
        bares.add(b);
        b=new Precio("lucas","85€","54");
        bares.add(b);
        b=new Precio("lucas","82€","52");
        bares.add(b);
        b=new Precio("lucas","85€","52");
        bares.add(b);*/
        ArrayList<Precio> bares = Datos_iniciales.Get_Lprecio();

        Listaprecios li=new Listaprecios(bares,this);
        ListView l=(ListView) findViewById(R.id.Bares_lista);
        l.setAdapter(li);
    }
}

package com.temcast.aplicacion_bares.Cargador_datos;

import com.temcast.aplicacion_bares.tipos.ListaCervezas;
import com.temcast.aplicacion_bares.tipos.Precio;

import java.util.ArrayList;

/**
 * Created by Jorge on 12/03/2017.
 */

public class Datos_iniciales {
    private static ArrayList<Precio> precio;
    private static ArrayList<ListaCervezas> listacerv;

    public  static ArrayList<Precio> Get_Lprecio() {
        if(precio==null){
            precio=new ArrayList<>();
            return precio;
        }
        return precio;
    }

    public  static ArrayList<ListaCervezas> getListaCervezas() {
        if(listacerv==null){
            listacerv= new ArrayList<>();
            return listacerv;
        }else {
        return listacerv;
        }
    }
}
